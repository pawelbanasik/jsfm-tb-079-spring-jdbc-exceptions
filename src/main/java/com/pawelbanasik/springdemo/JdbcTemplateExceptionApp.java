package com.pawelbanasik.springdemo;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;

import com.pawelbanasik.springdemo.dao.OrganizationDao;
import com.pawelbanasik.springdemo.domain.Organization;

public class JdbcTemplateExceptionApp {

	public static void main(String[] args) {

		// creating the application context
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans-cp.xml");

		// Create the bean
		OrganizationDao dao = (OrganizationDao) ctx.getBean("orgDao");

		List<Organization> orgs = null;

		// doing an exception on purpose
		// catching in application not DAO!
		try {
			orgs = dao.getAllOrganizations();
			// we can be more precise
		} catch (BadSqlGrammarException bge) {
			System.out.println("Sub exception message: " + bge.getMessage());
			System.out.println("Sub exception class: " + bge.getClass());

			// trying to catch this
		} catch (DataAccessException dae) {
			System.out.println("Exception message: " + dae.getMessage());
			System.out.println("Exception class: " + dae.getClass());
		}

		dao.getAllOrganizations();

		for (Organization org : orgs) {
			System.out.println(org);
		}

		// close the application context
		((ClassPathXmlApplicationContext) ctx).close();

	}

}
